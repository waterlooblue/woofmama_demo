$(document).ready(function () {
    $('#testimonials').slick({
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToShow: 3,
        appendArrows: '#testimonials',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
});